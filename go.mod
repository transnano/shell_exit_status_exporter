module gitlab.com/transnano/shell_exit_status_exporter

go 1.12

require (
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/common v0.12.0
	gopkg.in/yaml.v2 v2.3.0
)
